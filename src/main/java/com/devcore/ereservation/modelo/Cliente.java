package com.devcore.ereservation.modelo;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

// Clase que representa la tabla cliente

@Data
@Entity
@Table(name ="clientes")
@NamedQuery(name ="Cliente.findByIdentificacion", query = "SELECT c FROM Cliente c WHERE c.ClienteIdentificacion = ?1")
public class Cliente {
	
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name ="system-uuid", strategy = "uuid2")
	private String ClienteId;
	private String ClienteNombre;
	private String ClienteApellido;
	private String ClienteIdentificacion;
	private String ClienteDireccion;
	private String ClienteTelefono;
	private String ClienteEmail;
	
	@OneToMany(mappedBy = "cliente")  // Un cliente tiene muchas reservas
	private Set<Reserva> Reservas; // las relaciones se mapean bidireccionalmente
	
	
	public Cliente() {
		
	}

}
