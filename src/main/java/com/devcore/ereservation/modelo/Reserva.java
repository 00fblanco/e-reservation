package com.devcore.ereservation.modelo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import lombok.Data;

// Clase que representa la tabla Reserva

@Data
@Entity
@Table(name = "reservas")
public class Reserva {

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name ="system-uuid", strategy = "uuid2")
	private String ReservaId;
	
	@Temporal(TemporalType.DATE)
	private Date ReservaFechaIngreso;
	
	@Temporal(TemporalType.DATE)
	private Date ReservaFechaSalida;
	
	private int ReservaCantidadPersonas;
	
	private String ReservaDescripcion;
	
	@ManyToOne // una reserva tiene un cliente
	@JoinColumn(name = "ClienteId")
	private Cliente cliente; // este nombre debe ser el mismo que el mappedBy que en oneToMany
	
	
	
}
