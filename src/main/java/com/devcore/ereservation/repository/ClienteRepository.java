/**
 * 
 */
package com.devcore.ereservation.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcore.ereservation.modelo.Cliente;

/**
 * Interfaz para definir las operaciones de bd relacionadas con el cliente
 * 
 * @author Francisco Blanco
 *
 */
public interface ClienteRepository extends JpaRepository<Cliente, String>{
	
	//public List<Cliente> findByClienteApellido(String ClienteApellido); // findBy palabra reservada
	
	public Cliente findByIdentificacion(String ClienteIdentificacion); // named query declarajo en el POJO cliente
}
