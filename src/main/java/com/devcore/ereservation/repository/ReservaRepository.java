package com.devcore.ereservation.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcore.ereservation.modelo.Cliente;
import com.devcore.ereservation.modelo.Reserva;

public interface ReservaRepository extends JpaRepository<Reserva, String> {
	
	
	
	@Query("SELECT r FROM Reserva r where r.ReservaFechaIngreso =:fechaIngreso and r.ReservaFechaSalida = :fechaSalida")
	public List<Reserva> find(@Param("fechaIngreso") Date fechaIngreso, @Param("fechaSalida") Date fechaSalida);
	
}
