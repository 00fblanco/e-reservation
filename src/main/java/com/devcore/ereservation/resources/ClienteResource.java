/**
 * 
 */
package com.devcore.ereservation.resources;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcore.ereservation.modelo.Cliente;
import com.devcore.ereservation.resources.vo.ClienteVO;
import com.devcore.ereservation.services.ClienteService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * @author Fblanco Clase que representa el servicio web del cliente
 *
 */

@RestController
@RequestMapping(name = "/api/cliente")
@Api(tags = "cliente")
public class ClienteResource {

	private final ClienteService clienteService;

	public ClienteResource(ClienteService clienteService) {
		this.clienteService = clienteService;
	}

	
	@PostMapping(path = "/api/cliente")
	@ApiOperation(value = "Crear cliente", notes = "Servicio para crear un nuevo cliente")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Cliente creado correctamente"),
			@ApiResponse(code = 400, message = "Solicitud invalida") })
	public ResponseEntity<Cliente> createCliente(@RequestBody ClienteVO clienteVo) {
		Cliente cliente = new Cliente();
		cliente.setClienteNombre(cliente.getClienteNombre());
		cliente.setClienteApellido(clienteVo.getClienteApellido());
		cliente.setClienteDireccion(clienteVo.getClienteDireccion());
		cliente.setClienteIdentificacion(cliente.getClienteIdentificacion());
		cliente.setClienteTelefono(cliente.getClienteTelefono());
		cliente.setClienteEmail(cliente.getClienteEmail());
		return new ResponseEntity<>(this.clienteService.create(cliente), HttpStatus.CREATED);
	}

	@PutMapping(path = "/api/cliente/{ClienteIdentificacion}")
	@ApiOperation(value = "Actualizar cliente", notes = "Servicio para actualizar un cliente")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Cliente actualizado correctamente"),
			@ApiResponse(code = 400, message = "Cliente no encontrado") })
	public ResponseEntity<Cliente> updateCliente(@PathVariable("ClienteIdentificacion") String ClienteIdentificacion,
			@RequestBody ClienteVO clienteVo) {

		Cliente cliente = this.clienteService.findByIdentificacion(ClienteIdentificacion);
		if (cliente == null) {
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		}

		cliente.setClienteNombre(cliente.getClienteNombre());
		cliente.setClienteApellido(clienteVo.getClienteApellido());
		cliente.setClienteDireccion(clienteVo.getClienteDireccion());
		cliente.setClienteIdentificacion(cliente.getClienteIdentificacion());
		cliente.setClienteTelefono(cliente.getClienteTelefono());
		cliente.setClienteEmail(cliente.getClienteEmail());
		return new ResponseEntity<>(this.clienteService.update(cliente), HttpStatus.OK);
	}

	@DeleteMapping(path = "/api/cliente/{ClienteIdentificacion}")
	@ApiOperation(value = "Eliminar cliente", notes = "Servicio para eliminar un cliente")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Cliente eliminado correctamente"),
			@ApiResponse(code = 400, message = "Cliente no encontrado") })
	public void removeCliente(@PathVariable("ClienteIdentificacion") String ClienteIdentificacion) {
		Cliente cliente = this.clienteService.findByIdentificacion(ClienteIdentificacion);
		if (cliente != null) {
			this.clienteService.delete(cliente);
		}
	}

	@GetMapping(path = "/api/cliente")
	@ApiOperation(value = "Consultar clientes", notes = "Servicio para consultar los clientess")
	@ApiResponses(value = { @ApiResponse(code = 201, message = "Clientes encontrados"),
			@ApiResponse(code = 400, message = "Clientes no encontrados") })
	public ResponseEntity<List<Cliente>> findAll() {
		return ResponseEntity.ok(this.clienteService.findAll());
	}

}
