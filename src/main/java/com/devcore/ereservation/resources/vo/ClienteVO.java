package com.devcore.ereservation.resources.vo;


import lombok.Data;

@Data
public class ClienteVO {
	private String ClienteId;
	private String ClienteNombre;
	private String ClienteApellido;
	private String ClienteIdentificacion;
	private String ClienteDireccion;
	private String ClienteTelefono;
	private String ClienteEmail;
}
