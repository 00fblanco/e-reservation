/**
 * 
 */
package com.devcore.ereservation.services;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.devcore.ereservation.modelo.Cliente;
import com.devcore.ereservation.repository.ClienteRepository;

/**
 * 
 * Clase para definir los servicios del cliente
 * 
 * @author FBlanco
 *
 */

@Service
@Transactional(readOnly = true)
public class ClienteService {

	private final ClienteRepository clienteRepository;

	public ClienteService(ClienteRepository clienteRepository) {
		this.clienteRepository = clienteRepository;
	}

	// Metodo para realizar la operacion de guardar un cliente
	@Transactional
	public Cliente create(Cliente cliente) {
		return this.clienteRepository.save(cliente);
	}
	
	
	// Metodo para realizar la operacion de actualziar un cliente
	@Transactional
	public Cliente update(Cliente cliente) {
		return this.clienteRepository.save(cliente);
	}
	
	/**
	 * @param cliente
	 * Metodo para realizar la operacion de eliminar un cliente
	 */
	@Transactional
	public void delete(Cliente cliente) {
		this.clienteRepository.delete(cliente);
	}
	
	
	/**
	 * @param ClienteIdentificacion
	 * @return
	 * Metodo para consultar un cliente por su identificacion
	 */
	public Cliente findByIdentificacion(String ClienteIdentificacion) {
		return this.clienteRepository.findByIdentificacion(ClienteIdentificacion);
	}

	public List<Cliente> findAll() {
		return this.clienteRepository.findAll();
	}

}
